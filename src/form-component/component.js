import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import {} from '@polymer/polymer/lib/elements/dom-repeat.js';
import {} from '@polymer/polymer/lib/elements/dom-if.js';

// IMPORT CHILD COMPONENTS
import {} from '../text-component/component.js';
import {} from '../radio-component/component.js';
import {} from '../select-component/component.js';
import {} from '../date-component/component.js';

/**
 * @formComponent
 * A component that gets the data and then display's the appropriate fields base on the JSON config
 */
class formComponent extends PolymerElement {
  static get properties() {
    return {
      data: {
        type: Array,
        value() { 
          return [
            {
              fieldName: 'Name',
              uiLabel: 'Full Name',
              fieldType: 'text',
              status: 'active'
            },
            {
              fieldName: 'Sex',
              uiLabel: 'Gender',
              fieldType: 'radio-button',
              status: 'active'
            },
            {
              fieldName: 'DOB',
              uiLabel: 'Date of Birth',
              fieldType: 'date-picker',
              status: 'inactive'
            },
            {
              fieldName: 'Country',
              uiLabel: 'Country',
              fieldType: 'drop-down',
              status: 'active'
            }
          ]; 
        }
      },
      radioFields: {
        type: Array,
        value: function() {
          return [
            {
              value: "male",
              label: "Male"
            },
            {
              value: "female",
              label: "Female"
            }
          ];
        }
      },
      countries: {
        type: Array,
        value: function() {
          return [
            {
              name: "United States",
              value: "US"
            },
            {
              name: "Canada",
              value: "CA"
            },
            {
              name: "Mexico",
              value: "MX"
            }
          ]
        }
      }
    }
  }
  static get template() {
    return html`
      <style>
        .form-component {
          padding: 40px 0;
          width: 50%;
          margin: 0 auto;
          font-family: Arial, Helvetica, sans-serif;
          display: flex;
          flex-direction: column;
        }
        .submit-button {
          border: none;
          font-size: 1.25em;
          cursor: pointer;
          background-color: #0084e4;
          border-radius: 5px;
          padding: 10px 20px;
          color: #fff;
          transition: background-color ease-in-out 0.25s, box-shadow ease-in-out 0.2s;
          box-shadow: 2px 2px 5px #000;
          width: 100px;
          align-self: center;
          margin-top: 20px;
        }
        .submit-button:hover {
          background-color: #0761a2;
          box-shadow: 5px 5px 10px #000;
        }
      </style>
      <form class="form-component">
        <template is="dom-repeat" items="{{data}}">
          <template is="dom-if" if="[[_isEqualTo(item.status, 'active')]]">
            <template is="dom-if" if="[[_isEqualTo(item.fieldType, 'text')]]">
              <text-component field-label="[[item.uiLabel]]" field-name="[[item.fieldName]]"></text-component>
            </template>
            <template is="dom-if" if="[[_isEqualTo(item.fieldType, 'radio-button')]]">
              <radio-component field-label="[[item.uiLabel]]" field-name="[[item.fieldName]]" object-items="[[radioFields]]"></radio-component>
            </template>
            <template is="dom-if" if="[[_isEqualTo(item.fieldType, 'drop-down')]]">
              <select-component field-label="[[item.uiLabel]]" field-name="[[item.fieldName]]" object-items="[[countries]]"></select-component>
            </template>
            <template is="dom-if" if="[[_isEqualTo(item.fieldType, 'date-picker')]]">
              <date-component field-label="[[item.uiLabel]]" field-name="[[item.fieldName]]"></date-component>
            </template>
          </template>
        </template>
        <input class="submit-button" type="submit">
      </form>
    ` 
  }
  _isEqualTo(item, string) {
    return item === string;
  }
}

window.customElements.define('form-component', formComponent);