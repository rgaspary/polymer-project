import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * @customElement
 * @polymer
 */
class PolymerSampleApp extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          text-align: center;
          font-family: Arial, Helvetica, sans-serif;
        }
      </style>
      <h1>[[prop1]]!</h1>
    `;
  }
  static get properties() {
    return {
      prop1: {
        type: String,
        value: 'Polymer Problem Solution'
      }
    };
  }
}

window.customElements.define('polymer-sample-app', PolymerSampleApp);
