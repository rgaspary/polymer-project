import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * @textComponent
 * Stylized input text field component
 */
class textComponent extends PolymerElement {
  static get properties() {
    return {
      fieldLabel: {
        type: String
      },
      fieldName: {
        type: String
      }
    }
  }
  static get template() {
    return html`
      <style>
        fieldset {
          border: none;
          color: #333;
        }
        label {
          font-size: 1.25em;
        }
        input {
          font-size: 1.5em;
          border: none;
          border-bottom: 1px solid #cecece;
          padding: 0 10px;
        }
      </style>
      <fieldset>
        <label for="[[fieldName]]">[[fieldLabel]]</label>
        <input type="text" name="[[fieldName]]"/>
      </fieldset>
    `
  }
}

window.customElements.define('text-component', textComponent);