import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import {} from '@polymer/polymer/lib/elements/dom-repeat.js';

/**
 * @selectComponent
 * Stylized select group field component
 */
class selectComponent extends PolymerElement {
  static get properties() {
    return {
      fieldLabel: {
        type: String
      },
      fieldName: {
        type: String
      },
      objectItems: {
        type: Object
      }
    }
  }
  static get template() {
    return html`
      <style>
        fieldset {
          border: none;
          color: #333;
        }
        label {
          font-size: 1.25em;
        }
        select {
          border: none;
          background-color: #d8d8d8;
          font-size: 1.25em;
        }
      </style>
      <fieldset>
        <label for="[[fieldName]]">[[fieldLabel]]</label>
        <select name=[[fieldName]]>
          <option value="" selected>Choose one</option>
          <template is="dom-repeat" items="[[objectItems]]">
            <option value="[[item.value]]">[[item.name]]</option>
          </template>
        </select>
      </fieldset>
    `
  }
}

window.customElements.define('select-component', selectComponent);