import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * @dateComponent
 * Stylized date field component
 */
class dateComponent extends PolymerElement {
  static get properties() {
    return {
      fieldLabel: {
        type: String
      },
      fieldName: {
        type: String
      }
    }
  }
  static get template() {
    return html`
      <style>
        fieldset {
          border: none;
          color: #333;
        }
        label {
          font-size: 1.25em;
        }
        input {
          font-size: 1.25em;
          border: none;
          border-bottom: 1px solid #cecece;
          padding: 0 10px;
          font-family: Arial, Helvetica, sans-serif;
        }
      </style>
      <fieldset>
        <label for="[[fieldName]]">[[fieldLabel]]</label>
        <input type="date" name="[[fieldName]]">
      </fieldset>
    `
  }
}

window.customElements.define('date-component', dateComponent);