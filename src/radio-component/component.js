import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import {} from '@polymer/polymer/lib/elements/dom-repeat.js';

/**
 * @radioComponent
 * Stylized radio group field component
 */
class radioComponent extends PolymerElement {
  static get properties() {
    return {
      fieldLabel: {
        type: String
      },
      fieldName: {
        type: String
      },
      objectItems: {
        type: Object
      }
    }
  }
  static get template() {
    return html`
      <style>
        fieldset {
          border: none;
          color: #333;
        }
        label {
          font-size: 1.25em;
        }
        input {
          font-size: 1.5em;
        }
      </style>
      <fieldset>
        <label for="[[fieldName]]">[[fieldLabel]]</label>
        <template is="dom-repeat" items="[[objectItems]]">
          <input type="radio" name="[[fieldName]]" value="[[item.value]]">[[item.label]]</input>
        </template>
      </fieldset>
    `
  }
}

window.customElements.define('radio-component', radioComponent);